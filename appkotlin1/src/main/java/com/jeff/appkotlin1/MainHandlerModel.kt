package com.jeff.appkotlin1

import android.annotation.SuppressLint
import android.view.View
import androidx.lifecycle.ViewModel
import io.reactivex.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.exceptions.CompositeException
import io.reactivex.functions.BiFunction
import io.reactivex.internal.operators.single.SingleDefer
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.rxkotlin.toObservable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.lang.RuntimeException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.Exception

// http://mushuichuan.com/2015/12/11/rxjava-operator-6/

@SuppressLint("CheckResult")
class MainHandlerModel : ViewModel() {

    private fun printCurrentThread(tag: String = "") {
        val prefix = if (tag.isBlank()) {
            ""
        } else {
            "$tag: "
        }

        when (Thread.currentThread().name) {
            "main" -> Timber.i("${prefix}Thread(${Thread.currentThread().name})")
            else -> Timber.w("${prefix}Thread(${Thread.currentThread().name})")
        }
    }

    private val code = 0
    // 0: onComplete
    // 1: onSuccess
    // 2: onNext
    // 3: onError

    private var disposable: Disposable? = null

    private var mEmitter13: Emitter<String>? = null
    private var mEmitter14: Emitter<String>? = null

    init {
        Observable.create(ObservableOnSubscribe<String> {
                    mEmitter13 = it
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(10, TimeUnit.SECONDS)
                .subscribeBy(
                        onNext = {
                            Timber.d(it)
                        },
                        onComplete = {

                        }
                )

        Observable.create(ObservableOnSubscribe<String> {
                    mEmitter14 = it
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .throttleLatest(10, TimeUnit.SECONDS)
                .subscribeBy(
                        onNext = {
                            Timber.d(it)
                        },
                        onComplete = {

                        }
                )


    }

    fun onClickedBtn1(view: View) {

        Observable.interval(2, 4, TimeUnit.SECONDS, Schedulers.io())
                .flatMap {
                    if (it % 2 == 0L) {
                        return@flatMap Observable.just("1")
                    } else {
                        return@flatMap Single.just("2").flatMapObservable { s ->
                            return@flatMapObservable Observable.just(s)
                        }
                    }
                }
                .subscribe { result ->
                    Timber.d("R: $result")
                }

        /*
        .takeWhile { count < 5 }
        .observeOn(Schedulers.io())
        .subscribe(object : Observer<Long> {
            override fun onSubscribe(d: Disposable) {
                Timber.d("onSubscribe")
                disposable = d
            }

            override fun onNext(l: Long) {
                count = l
                Timber.d("onNext: $l")
            }

            override fun onComplete() {
                Timber.d("onComplete")
            }

            override fun onError(e: Throwable) {
                Timber.e("onError")
            }
        })
        */
    }

    fun onClickedBtn2(view: View) {
        Timber.w("Disposable: ${disposable?.isDisposed}")

        Observable.interval(2, TimeUnit.SECONDS, Schedulers.newThread())
                .observeOn(Schedulers.io())
                .subscribeBy(
                        onNext = {
                            Timber.d("onNext")
                        },
                        onComplete = {
                            Timber.d("onComplete")
                        },
                        onError = {
                            Timber.e("onError")
                        }
                )
    }

    /**
     * takeWhile
     */
    fun onClickedBtn3(view: View) {

        var count = 0

        Observable.interval(3, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe {
                    Timber.d("doOnSubscribe")
                }
                .map {
                    Timber.i("Map: ${++count}")
                    count
                }
                .takeWhile { count < 5 }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            Timber.d("onNext: $it")
                        },
                        onComplete = {
                            Timber.d("onComplete")
                        },
                        onError = {
                            Timber.e("onError: $it")
                        })
    }

    /**
     * map:
     * flatMap:
     * contactMap:
     * https://www.zhihu.com/question/32209660
     */
    fun onClickedBtn4(view: View) {

        val list1 = mutableListOf<String>()
        for (i in 0..5) {
            list1.add(i.toString())
        }

        val map = mutableMapOf<String, MutableList<String>>()
        for (first in list1) {
            val list = arrayListOf<String>()

            for (i in 6..9) {
                list.add("$first, $i")
            }

            map[first] = list
        }

        list1.toObservable().flatMap {
            Timber.w("flatMap: $it")
            map[it]?.toObservable()
        }.subscribeBy(
                onNext = {
                    Timber.d("onNext: $it")
                },
                onComplete = {
                    Timber.i("onComplete")
                },
                onError = {
                    Timber.e("onError: $it")
                })
    }

    fun onClickedBtn5(view: View) {
        Observable.interval(2, TimeUnit.SECONDS, Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(5, TimeUnit.SECONDS)
                .subscribeBy(
                        onNext = {
                            Timber.d("onNext: $it")
                        },
                        onComplete = {
                            Timber.i("onComplete")
                        },
                        onError = {
                            Timber.e("onError: $it")
                        })
    }

    fun onClickedBtn6(view: View) {
        /*
        Flowable.create(FlowableOnSubscribe<String> {

        }, BackpressureStrategy.BUFFER).subscribeBy{

        }
        */
        // 能夠發射0或n個數據，並以成功或錯誤事件終止。支持Backpressure，可以控制數據源發射的速度。
        Flowable.create<String>({ emitter ->
            when (code) {
                0 -> emitter.onComplete()
                3 -> emitter.onError(Exception("No"))
                2 -> emitter.onNext("Yes")
            }
        }, BackpressureStrategy.BUFFER).subscribeBy(
                onNext = {
                    Timber.d("onNext: $it")
                },
                onComplete = {
                    Timber.i("onComplete")
                },
                onError = {
                    Timber.e("onError: $it")
                })
    }

    fun onClickedBtn7(view: View) {
        // 能夠發射0或n個數據，並以成功或錯誤事件終止。
        Observable.create<String> { emitter ->

        }.subscribeBy(
                onNext = {
                    Timber.d("onNext: $it")
                },
                onComplete = {
                    Timber.i("onComplete")
                },
                onError = {
                    Timber.e("onError: $it")
                })
    }

    fun onClickedBtn8(view: View) {
        // 發射單個數據或錯誤事件。
        Single.create<String> { emitter ->
                    when (code) {
                        1 -> emitter.onSuccess("Yes")
                        3 -> emitter.onError(Exception("No"))
                    }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onSuccess = {
                            Timber.d("onSuccess: $it")
                        },
                        onError = {
                            Timber.e("onError: $it")
                        }
                )
    }

    fun onClickedBtn9(view: View) {
        // 能夠發射0或者1個數據，完成或失敗。可以看成是Single和Completable的结合。
        Maybe.create<String> { emitter ->
            when (code) {
                1 -> emitter.onSuccess("Yes")
                0 -> emitter.onComplete()
                3 -> emitter.onError(Exception("No"))
            }
        }.subscribeBy(
                onSuccess = {
                    Timber.d("onSuccess: $it")
                },
                onComplete = {
                    Timber.i("onComplete")
                },
                onError = {
                    Timber.e("onError: $it")
                }
        )
    }

    fun onClickedBtn10(view: View) {
        // 不發射數據，處理 onComplete 和 onError 事件。
        Completable.create { emitter ->
            when (code) {
                0 -> emitter.onComplete()
                3 -> emitter.onError(Exception("No"))
            }
        }.subscribeBy(
                onComplete = {
                    Timber.i("onComplete")
                },
                onError = {
                    Timber.e("onError: $it")
                }
        )
    }

    private fun getHi(content: String): Single<String> {
        return Single.create<String> { emitter ->
            try {
                Timber.d("Start, $content")
                Thread.sleep(10 * 1000)
                Timber.d("Stop, $content")
                emitter.onSuccess("Hi, $content")
            } catch (e: Exception) {
                Timber.e(e.message)
            } finally {
            }
        }
    }

    fun onClickedBtn11(view: View) {
        var c = 0

        getHi("Btn11")
                .subscribeOn(Schedulers.io())
                .timeout(2, TimeUnit.SECONDS)
                .retryWhen { r ->
                    r.flatMapSingle { e ->
                        if (c < 2) {
                            Single.fromCallable {
                                c++
                                Timber.w("Retry...$c. ${e.message}")
                            }
                        } else {
                            Timber.w("Retry stop: ${e.message}")
                            Single.error(e)
                        }
                    }
                }
                .subscribeBy(
                        onSuccess = {
                            Timber.d("Finish")
                        },
                        onError = {
                            Timber.e(it)
                        }
                )
    }

    fun onClickedBtn12(view: View) {
        getHi("Jeff").flatMap {
                    Timber.d(it)
                    return@flatMap getHi("Yu")
                }
                .subscribeBy(
                        onSuccess = {
                            Timber.d(it)
                            Timber.w("Finish")
                        },
                        onError = {
                            Timber.e(it)
                        }
                )
    }

    fun onClickedBtn13(view: View) {
        Calendar.getInstance().time.let {
            SimpleDateFormat("HH:mm:ss").format(it)
        }.let {
            Timber.d("onClickedBtn13: $it")
            mEmitter13!!.onNext(it)
        }
    }

    fun onClickedBtn14(view: View) {
        Calendar.getInstance().time.let {
            SimpleDateFormat("HH:mm:ss").format(it)
        }.let {
            Timber.d("onClickedBtn14: $it")
            mEmitter14!!.onNext(it)
        }
    }

    // Btn15
    private fun singleString(str: String) = Single.create<String> { emitter ->
        printCurrentThread()
        Timber.v("singleString")
        emitter.onSuccess(str)
    }

    private fun singleInt(n: Int) = Single.create<Int> { emitter ->
        printCurrentThread()
        Timber.v("singleInt")
        emitter.onSuccess(n)
    }

    fun onClickedBtn15(view: View) {
        Single.zip(singleString("Btn15")
                        , singleInt(999)
                        , BiFunction<String, Int, String> { str, n ->
                    "$n, $str"
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy {
                    printCurrentThread()
                    Timber.d(it)
                }
    }

    // Btn16
    private fun singleError() = Single.create<Int> { emitter ->
        printCurrentThread()
        Timber.v("singleError")
        emitter.onError(RuntimeException("Btn16 fail"))
    }

    fun onClickedBtn16(view: View) {
        Single.zip(singleString("Btn16")
                        , singleError()
                        , BiFunction<String, Int, String> { str, n ->
                    "$n, $str"
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onSuccess = {
                            printCurrentThread()
                            Timber.d(it)
                        },
                        onError = {
                            printCurrentThread()
                            Timber.e(it)
                        }
                )
    }

    fun onClickedBtn17(view: View) {
        /*
        Single.create<String> { emitter ->
            emitter.onError(RuntimeException("Btn17_1"))
        }
                .subscribeOn(Schedulers.io())
                .onErrorReturn {
                    printCurrentThread()
                    "onErrorReturn"
                }
                .subscribeBy {
                    printCurrentThread()
                    Timber.d(it)
                }
         */

        Single.create<String> { emitter ->
                    printCurrentThread("Btn17_2 (emitter)")
                    emitter.onError(RuntimeException("Btn17_2"))
                }
                .onErrorResumeNext {
                    printCurrentThread("Btn17_2 (onErrorResumeNext)")
                    Single.just("onErrorResumeNext")
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy {
                    printCurrentThread("Btn17_2 (subscribeBy)")
                    Timber.d(it)
                }
    }

    fun onClickedBtn18(view: View) {
        Observable.create<Long> { emitter ->
                    for (i in 101..108) {
                        Thread.sleep(300L)
                        emitter.onNext(i.toLong())
                    }
                }
                //.timeInterval()
                .timestamp()
                .subscribe {
                    Timber.d("$it")
                }
    }

    // Btn19
    private fun getSingleObservable(tag: String) = Single.create<String> { emitter ->
        printCurrentThread(tag)
        emitter.onSuccess(tag)
    }

    fun onClickedBtn19(view: View) {
        Single.create<String> { emitter ->
                    "Btn19 (1)".also { v ->
                        printCurrentThread(v)
                        emitter.onSuccess(v)
                    }
                }
                .flatMap {
                    printCurrentThread("~~$it")
                    getSingleObservable("Btn19 (2)")
                            .subscribeOn(AndroidSchedulers.mainThread())
                }
                .map {
                    printCurrentThread("~~$it")
                    "Btn19 (3)".also { v ->
                        printCurrentThread(v)
                    }
                }
                .flatMap {
                    printCurrentThread("~~$it")
                    getSingleObservable("Btn19 (4)")
                }
                .flatMap {
                    printCurrentThread("~~$it")
                    getSingleObservable("Btn19 (5)")
                            .subscribeOn(Schedulers.io())
                }
                .flatMap {
                    printCurrentThread("~~$it")
                    getSingleObservable("Btn19 (6)")
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy {
                    printCurrentThread("Btn19 (subscribeBy)")
                }
    }

    var disposable2: Disposable? = null

    fun onClickedBtn20(view: View) {
        SingleDefer {
            try {
                Thread.sleep(10000L)
            } catch (e: Exception) {
                Timber.e("Catch: $e")
                //Timber.e(e)
            }
            Single.just("Btn20 Finish")
        }
                .subscribeOn(Schedulers.io())
                .doOnSubscribe {
                    Timber.d("Btn20 doOnSubscribe")
                }
                .timeout(5, TimeUnit.SECONDS)
                //.delay(10, TimeUnit.SECONDS)
                .subscribeBy(
                        onSuccess = {
                            Timber.d(it)
                        },
                        onError = {
                            Timber.e("onError: ${it.message}")
                            //Timber.e(it)
                        }
                ).also {
                    disposable2 = it
                }
    }

    fun onClickedBtn21(view: View) {
        if (disposable2 != null) {
            Timber.i("Disposable2: ${disposable2?.isDisposed}")
        } else {
            Timber.w("Please clicked Btn20")
        }
    }

    // https://blog.csdn.net/johnny901114/article/details/51568562
    // https://maxwell-nc.github.io/android/rxjava2-4.html
    fun onClickedBtn22(view: View) {
        val single1 = Single.create<String> {
            Thread.sleep(3000L)
            it.onSuccess("One")
            //it.onError(Exception("One"))
        }

        val single2 = Single.create<String> {
            Thread.sleep(5000L)
            //it.onSuccess("Two")
            it.onError(Exception("Two"))
        }

        //Single.merge(single1, single2)
        Single.mergeDelayError(
                        single1.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()),
                        single2.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()))
                .doFinally {
                    printCurrentThread("doFinally")
                }
                .subscribeBy(
                        onNext = {
                            printCurrentThread("onNext")
                            Timber.d("onNext: $it")
                        },
                        onComplete = {
                            printCurrentThread("onComplete")
                        },
                        onError = {
                            printCurrentThread("onError")

                            if (it is CompositeException) {
                                it.exceptions[0]

                                Timber.w("is CompositeException")
                            }
                            Timber.e("onError: $it")
                        }
                )

    }

    fun onClickedBtn23(view: View) {
        Observable.fromCallable {
            Timber.i("Start")
        }.subscribeOn(Schedulers.io())
            .doOnNext {
                // 必須切換到'非'主執行緒, doOnNext才不會卡住發射源.
                Observable.fromCallable {
                    Timber.i("DoOnNext Start...")
                    Thread.sleep(3000L)
                    Timber.i("DoOnNext Finish...")
                }.subscribeOn(Schedulers.io()).subscribe()
            }.subscribe {
                Timber.i("Finish")
            }
    }

    // https://blog.csdn.net/jdsjlzx/article/details/51685769
    fun onClickedBtn24(view: View) {
        Single.fromCallable {
            "fromCallable 0".also {
                printCurrentThread(it)
            }
        }
            // subscribeOn決定初始執行緒(只有第一個作用)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map {
                "map 1".also {
                    printCurrentThread(it)
                }
            }
            // observeOn指定以下執行緒
            .observeOn(Schedulers.io())
            .map {
                "map 2".also {
                    printCurrentThread(it)
                }
            }
            .map {
                "map 3".also {
                    printCurrentThread(it)
                }
            }
            .subscribeBy {
                printCurrentThread("subscribeBy: $it")
            }
    }
}