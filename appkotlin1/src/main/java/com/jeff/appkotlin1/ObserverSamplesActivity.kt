package com.jeff.appkotlin1

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import timber.log.Timber

class ObserverSamplesActivity : AppCompatActivity(R.layout.activity_samples) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        findViewById<Button>(R.id.btn1).setOnClickListener {
            ObserverSamples.getDebounce().subscribe(getObserverLong())
        }

        findViewById<Button>(R.id.btn2).setOnClickListener {
            ObserverSamples.getThrottleLatest().subscribe(getObserverLong())
        }

        findViewById<Button>(R.id.btn3).setOnClickListener {
            ObserverSamples.getThrottleFirst().subscribe(getObserverLong())
        }
    }

    private fun getObserverLong(): Observer<Long> {
        return object : Observer<Long> {
            override fun onSubscribe(d: Disposable) {
                Timber.d("onSubscribe")
            }

            override fun onNext(l: Long) {
                Timber.d("onNext: $l")
            }

            override fun onComplete() {
                Timber.d("onComplete")
            }

            override fun onError(e: Throwable) {
                Timber.e("onError")
            }
        }
    }

    private fun getObserverString(): Observer<String> {
        return object : Observer<String> {
            override fun onSubscribe(d: Disposable) {
                Timber.d("onSubscribe")
            }

            override fun onNext(s: String) {
                Timber.d("onNext: $s")
            }

            override fun onComplete() {
                Timber.d("onComplete")
            }

            override fun onError(e: Throwable) {
                Timber.e("onError")
            }
        }
    }
}
