package com.jeff.appkotlin1

import android.view.View
import androidx.appcompat.app.AppCompatActivity

// 操作符列表
// https://www.zhihu.com/question/32209660

// 進階
// http://akarnokd.blogspot.com

// 分析
// https://blog.csdn.net/TellH/article/details/71534704

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    fun onBtnClicked(view: View) {
        val viewModel = MainHandlerModel()
        when (view.tag) {
            "1" -> viewModel.onClickedBtn1(view)
            "2" -> viewModel.onClickedBtn2(view)
            "3" -> viewModel.onClickedBtn3(view)
            "4" -> viewModel.onClickedBtn4(view)
            "5" -> viewModel.onClickedBtn5(view)
            "6" -> viewModel.onClickedBtn6(view)
            "7" -> viewModel.onClickedBtn7(view)
            "8" -> viewModel.onClickedBtn8(view)
            "9" -> viewModel.onClickedBtn9(view)
            "10" -> viewModel.onClickedBtn10(view)
            "11" -> viewModel.onClickedBtn11(view)
            "12" -> viewModel.onClickedBtn12(view)
            "13" -> viewModel.onClickedBtn13(view)
            "14" -> viewModel.onClickedBtn14(view)
            "15" -> viewModel.onClickedBtn15(view)
            "16" -> viewModel.onClickedBtn16(view)
            "17" -> viewModel.onClickedBtn17(view)
            "18" -> viewModel.onClickedBtn18(view)
            "19" -> viewModel.onClickedBtn19(view)
            "20" -> viewModel.onClickedBtn20(view)
            "21" -> viewModel.onClickedBtn21(view)
            "22" -> viewModel.onClickedBtn22(view)
            "23" -> viewModel.onClickedBtn23(view)
            "24" -> viewModel.onClickedBtn24(view)
        }
    }
}
