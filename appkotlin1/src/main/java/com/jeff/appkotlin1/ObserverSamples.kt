package com.jeff.appkotlin1

import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import timber.log.Timber
import java.util.concurrent.TimeUnit

object ObserverSamples {

    // https://maxwell-nc.github.io/android/rxjava2-3.html#throttleWithTimeout_/_debounce

    // debounce / throttleWithTimeout: 等Timeout時間到, 才發送最後一筆資料.
    fun getDebounce(): Observable<Long> = Observable.create<Long> { emitter ->
        for (i in 0..10) {
            emitter.onNext(i.toLong())
        }
    }.throttleWithTimeout(5, TimeUnit.SECONDS)
    //.debounce(5, TimeUnit.SECONDS)

    // throttleLatest: 會先發送第一筆資料, 等Timeout時間到, 在發送最後一筆資料.
    fun getThrottleLatest(): Observable<Long> = Observable.create<Long> { emitter ->
        for (i in 0..10) {
            emitter.onNext(i.toLong())
        }
    }.throttleLatest(5, TimeUnit.SECONDS)

    // throttleFirst: 只發送第一筆資料, 等Timeout時間過才發送之後的資料.
    fun getThrottleFirst(): Observable<Long> = Observable.create<Long> { emitter ->
        for (i in 0..10) {
            emitter.onNext(i.toLong())
            Thread.sleep(1000L)
        }
    }.throttleFirst(3, TimeUnit.SECONDS)

    private fun getConsumer_without_lambda(): Consumer<String> {
        return object : Consumer<String> {
            override fun accept(string: String?) {
                Timber.d("accept: $string")
            }
        }
    }

    private fun getConsumer() = Consumer<String> {
        Timber.d("accept: $it")
    }

    private fun getSingle() = Single.create<String> {
        it.onSuccess("getSingle")
    }
}