package com.jeff.rxjavasample;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class ConcatExampleActivity extends AppCompatActivity {
    // private static final String TAG = ConcatExampleActivity.class.getSimpleName();

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_example);

        textView = findViewById(R.id.textView);
    }

    public void onClick1(View view) {
        doSomeWork(1);
    }

    public void onClick2(View view) {
        doSomeWork(2);
    }

    public void onClick3(View view) {
        doSomeWork(3);
    }

    public void onClickClean(View view) {
        textView.setText("");
    }

    /*
     * Using concat operator to combine Observable : concat maintain
     * the order of Observable.
     * It will emit all the 7 values in order
     * here - first "A1", "A2", "A3", "A4" and then "B1", "B2", "B3"
     * first all from the first Observable and then
     * all from the second Observable all in order
     */
    private void doSomeWork(int aCase) {
        final String[] aStrings = {"A1", "A2", "A3", "A4"};
        final String[] bStrings = {"B1", "B2", "B3"};
        final Observable<String> aObservable = Observable.fromArray(aStrings);
        final Observable<String> bObservable = Observable.fromArray(bStrings);

        Observable<String> myObservable1 = Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> emitter) throws Exception {
                emitter.onNext("Jeff1");
                emitter.onComplete();
                emitter.onNext("Jeff2");
            }
        });

        Observable<String> myObservable2 = Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> emitter) throws Exception {
                emitter.onNext("JeffA");
                emitter.onError(new Exception("OH !"));
                emitter.onNext("JeffB");
            }
        });

        Observable<String> myObservable3 = Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> emitter) throws Exception {
                emitter.onNext("Jeff");
            }
        });

        switch (aCase) {
            case 1:
                Observable.concat(myObservable1, aObservable, bObservable).subscribe(myObserver);
                break;
            case 2:
                Observable.concat(myObservable2, aObservable, bObservable).subscribe(myObserver);
                break;
            case 3:
                Observable.concat(myObservable3, aObservable, bObservable).subscribe(myObserver);
                break;
        }
    }

    private Observer<String> myObserver = getObserver();

    private Observer<String> getObserver() {
        return new Observer<String>() {

            Disposable disposable;

            @Override
            public void onSubscribe(Disposable d) {
                textView.append("onSubscribe: " + d.isDisposed());
                textView.append("\n");
                Timber.d(" onSubscribe: %s", d.isDisposed());

                // 強制解除訂閱: disposable.dispose();
                disposable = d;
            }

            @Override
            public void onNext(String value) {
                textView.append("onNext : value : " + value);
                textView.append("\n");
                Timber.d(" onNext : value : %s", value);
            }

            @Override
            public void onError(Throwable e) {
                textView.append("onError : " + e.getMessage());
                textView.append("\n");
                Timber.e(" onError: %s", e.getMessage());
            }

            @Override
            public void onComplete() {
                textView.append("onComplete");
                textView.append("\n");
                Timber.d("onComplete");
            }
        };
    }
}
