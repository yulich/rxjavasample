package com.jeff.logapplication;

import org.jetbrains.annotations.Nullable;

import timber.log.Timber;

public class DebugLogTree extends Timber.DebugTree {

    private static final String TAG_GLOBAL = "Jeff";

    private static final String FORMAT_MESSAGE = "%s: %s";

    private static final String FORMAT_ELEMENT = "(%s:%d)[%s]";

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        message = String.format(FORMAT_MESSAGE, tag, message);

        super.log(priority, TAG_GLOBAL, message, t);
    }

    @Override
    protected String createStackElementTag(StackTraceElement element) {
        return String.format(FORMAT_ELEMENT, element.getFileName(), element.getLineNumber(), element.getMethodName());
    }

    @Override
    protected boolean isLoggable(@Nullable String tag, int priority) {
        return super.isLoggable(tag, priority);
    }
}