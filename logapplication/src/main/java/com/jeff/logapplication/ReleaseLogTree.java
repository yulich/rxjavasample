package com.jeff.logapplication;

import android.util.Log;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import timber.log.Timber;

public class ReleaseLogTree extends Timber.Tree {

    @Override
    protected boolean isLoggable(String tag, int priority) {
        // Don't log VERBOSE, DEBUG and INFO
        if (priority == Log.VERBOSE || priority == Log.DEBUG || priority == Log.INFO) {
            return false;
        }

        // Log only ERROR, WARN and WTF
        return true;

    }

    @Override
    protected void log(int priority, @Nullable String tag, @NotNull String message, @Nullable Throwable t) {
        if (!isLoggable(tag, priority)) {
            return;
        }

        // Fabric
        /*
        CrashReporter.log(priority, tag, message);

        if (t != null) {
            if (priority == Log.ERROR) {
                CrashReporter.logError(t);

            } else if (priority == Log.WARN) {
                CrashReporter.logWarning(t);
            }
        }*/
    }
}