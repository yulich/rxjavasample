package com.jeff.app2;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.jeff.app2.databinding.ActivityMainBinding;

/**
 * https://blog.csdn.net/donkor_/article/details/79709366
 */
public class MainActivity extends AppCompatActivity {

    ActivityMainBinding activityMainBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        activityMainBinding.setMainHandler(new MainHandlerModel());
        activityMainBinding.getMainHandler().init5();
    }
}
