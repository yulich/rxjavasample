package com.jeff.app2;

import android.view.View;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class MainHandlerModel {

    public void onClickedBtn1(View view) {

        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            list.add(String.valueOf(i));
        }

        Observable.fromIterable(list)
                .flatMap(new Function<String, Observable<Integer>>() {
                    @Override
                    public Observable<Integer> apply(String s) throws Exception {
                        return Observable.fromArray(Integer.valueOf(s));
                    }
                })
                .filter(new Predicate<Integer>() {
                    @Override
                    public boolean test(Integer integer) throws Exception {
                        if (integer % 3 == 0) {
                            return false;
                        }

                        return true;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Integer>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Timber.d("onSubscribe");
                    }

                    @Override
                    public void onNext(Integer i) {
                        Timber.d("OnNext: %d", i);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e("onError: %s", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Timber.d("onComplete");
                    }
                });
    }

    CompositeDisposable compositeDisposable2 = new CompositeDisposable();

    public void onClickedBtn2(View view) {
        compositeDisposable2.clear();

        String[] words = {"Hello", "Hi", "Aloha"};

        Observable.fromArray(words)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                //.delay(5, TimeUnit.SECONDS)
                .subscribe(observable2);
    }

    Observer<String> observable2 = new Observer<String>() {
        @Override
        public void onSubscribe(Disposable d) {
            Timber.d("onSubscribe");

            compositeDisposable2.add(d);
        }

        @Override
        public void onNext(String s) {
            Timber.d("OnNext: %s", s);
        }

        @Override
        public void onError(Throwable e) {
            Timber.e("onError: %s", e.getMessage());
        }

        @Override
        public void onComplete() {
            Timber.d("onComplete");
        }
    };

    public void onClickedBtn3(View view) {
        compositeDisposable2.clear();

        Observable<String> observable3 = Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> emitter) {
                for (int i = 0; i < 10; i++) {
                    emitter.onNext(String.valueOf(i));
                }

                emitter.onComplete();
            }
        });

        observable3
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Timber.d("onSubscribe");

                        compositeDisposable2.add(d);
                    }

                    @Override
                    public void onNext(String s) {
                        Timber.d("OnNext: %s", s);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e("onError: %s", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Timber.d("onComplete");
                    }
                });

    }

    public void onClickedBtn4(View view) {
        compositeDisposable2.clear();

        // Observer
        /*
        compositeDisposable2.add(
                Observable.interval(1, TimeUnit.SECONDS, Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Observer<Long>() {
                                    @Override
                                    public void onSubscribe(Disposable d) {
                                        Timber.d("onSubscribe");

                                        compositeDisposable2.add(d);
                                    }

                                    @Override
                                    public void onNext(Long aLong) {
                                        if (aLong.intValue() == 2) {
                                            compositeDisposable2.clear();
                                        }
                                        Timber.d("OnNext: %d", aLong);
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Timber.e("onError: %s", e.getMessage());
                                    }

                                    @Override
                                    public void onComplete() {
                                        Timber.d("onComplete");
                                    }
                                });
                                */

        // Consumer
        compositeDisposable2.add(
                Observable.interval(1, TimeUnit.SECONDS, Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Consumer<Long>() {
                                    @Override
                                    public void accept(Long aLong) throws Exception {
                                        if (aLong.intValue() == 0) {
                                            compositeDisposable2.clear();
                                        }
                                        Timber.d("OnAccept: %d", aLong);
                                    }
                                }));
    }

    public void init5() {
        RxBus.getInstance().toObservable(String.class).subscribe(new Consumer<String>() {
            @Override
            public void accept(String s) throws Exception {
                Timber.d("OnAccept: %s", s);
            }
        });

        RxBus.getInstance().toObservable(String.class).subscribe(new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {
                Timber.d("onSubscribe");
            }

            @Override
            public void onNext(String s) {
                Timber.d("OnNext: %s", s);
            }

            @Override
            public void onError(Throwable e) {
                Timber.e("onError: %s", e.getMessage());
            }

            @Override
            public void onComplete() {
                Timber.d("onComplete");
            }
        });
    }

    public void onClickedBtn5(View view) {

        RxBus.getInstance().post("Jeff");
    }

    public void onClickedBtn6(View view) {

        RxBus.getInstance().post("Jeff");
    }

    public void onClickedBtn7(View view) {

        RxBus.getInstance().post("Jeff");
    }

    public void onClickedBtn8(View view) {

        RxBus.getInstance().post("Jeff");
    }

    public void onClickedBtn9(View view) {

        RxBus.getInstance().post("Jeff");
    }

    public void onClickedBtn10(View view) {

        RxBus.getInstance().post("Jeff");
    }

    public void onClickedBtn11(View view) {

        RxBus.getInstance().post("Jeff");
    }

    public void onClickedBtn12(View view) {

        RxBus.getInstance().post("Jeff");
    }
}
